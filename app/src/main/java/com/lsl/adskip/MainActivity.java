package com.lsl.adskip;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.lsl.adskip.service.MyService;

public class MainActivity extends AppCompatActivity {

    private ImageButton imageBtn;
    private TextView tipView;
    private Switch switchButton;
    private Switch dialogSwitch;
    private Switch superModeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        imageBtn = findViewById(R.id.imageBtn);
        tipView = findViewById(R.id.tip);
        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
            }
        });
        switchButton = findViewById(R.id.switch_button);
        SharedPreferences sharedPreferences = getSharedPreferences("serviceState", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // 状态改变时的逻辑处理
                if (isChecked) {
                    // 开启屏蔽广告
                    if (MyService.isServiceEnable()) {
                        MyService.serviceState = true;
                        switchButton.setChecked(true);
                        editor.putBoolean("serviceState", true);
                    } else {
                        switchButton.setChecked(false);
                        showDialog();
                    }
                } else {
                    // 开关关闭时的操作
                    MyService.serviceState = false;
                    switchButton.setChecked(false);
                    editor.putBoolean("serviceState", false);
                }
                editor.apply();
            }
        });
        dialogSwitch = findViewById(R.id.switch_dialog);
        SharedPreferences dialogState = getSharedPreferences("dialogAD", Context.MODE_PRIVATE);
        SharedPreferences.Editor adEditor = dialogState.edit();
        dialogSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (MyService.isServiceEnable()) {
                        adEditor.putBoolean("dialogAD", true);
                        MyService.dialogAD = true;
                        dialogSwitch.setChecked(true);
                    } else {
                        dialogSwitch.setChecked(false);
                        showDialog();
                    }
                } else {
                    adEditor.putBoolean("dialogAD", false);
                    MyService.dialogAD = false;
                    dialogSwitch.setChecked(false);
                }
                adEditor.apply();
            }
        });

        superModeBtn = findViewById(R.id.superMode);
        SharedPreferences superSwitch = getSharedPreferences("superSwitch",Context.MODE_PRIVATE);
        SharedPreferences.Editor superEditor = superSwitch.edit();
        superModeBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (MyService.isServiceEnable()){
                        MyService.superMode = true;
                        superEditor.putBoolean("superSwitch",true);
                        showSuperTip();
                    }else{
                        superModeBtn.setChecked(false);
                        showDialog();
                    }
                }else{
                    superEditor.putBoolean("superSwitch",false);
                    MyService.superMode = false;
                }
                superEditor.apply();
            }
        });
    }

    @Override
    public void onResume() {
        if (MyService.isServiceEnable()) {
            tipView.setText("无障碍服务已启动");
            imageBtn.setImageResource(R.drawable.laugh);
            SharedPreferences sharedPreferences = getSharedPreferences("serviceState", Context.MODE_PRIVATE);
            boolean state = sharedPreferences.getBoolean("serviceState", false);
            MyService.serviceState = state;
            boolean dialogState = getSharedPreferences("dialogAD", Context.MODE_PRIVATE).getBoolean("dialogAD", false);
            MyService.dialogAD = dialogState;
            dialogSwitch.setChecked(dialogState);
            switchButton.setChecked(state);
            boolean superModeState = getSharedPreferences("superSwitch",Context.MODE_PRIVATE).getBoolean("superSwitch",false);
            superModeBtn.setChecked(superModeState);
            MyService.superMode = superModeState;
        } else {
            tipView.setText(R.string.tip);
            imageBtn.setImageResource(R.drawable.cray);
            dialogSwitch.setChecked(false);
            switchButton.setChecked(false);
            MyService.dialogAD = false;
            MyService.serviceState = false;
            showDialog();
        }
        super.onResume();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("服务状态");
        builder.setMessage("无障碍服务未开启，无法使用，请进入系统无障碍服务中找到本APP，开启服务。温馨提示：本应用没有网络功能，也无需授权手机隐私相关权限，您的隐私是百分百安全的，请放心使用！");
        builder.setPositiveButton("去设置", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast toast = Toast.makeText(MainActivity.this, "您已取消，如需正常使用，请点击熊猫头", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showSuperTip(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("服务说明");
        builder.setMessage("你已开启超级模式，该模式收录了几百种APP的弹窗匹配规则，原理是监听弹窗类型，并自动帮你点击对应按钮，如你在使用其他App过程中出现了意料之外的效果，请关闭该模式");
        builder.setNegativeButton("我已知晓", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}