package com.lsl.adskip.entity;

import java.util.ArrayList;

public class RuleEntity {

    private ArrayList<RuleDetail> rules;

    public ArrayList<RuleDetail> getRules() {
        return rules;
    }

    public void setRules(ArrayList<RuleDetail> rules) {
        this.rules = rules;
    }
}
