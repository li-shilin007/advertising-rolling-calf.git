package com.lsl.adskip.entity;

public class RuleDetail {
    String id;
    String action;

    public RuleDetail(String id,String action){
        this.id = id;
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
