package com.lsl.adskip.util;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    public static void showShortToast(Context context,String msg){
        Toast toast = Toast.makeText(context,msg,Toast.LENGTH_SHORT);
        toast.show();
    }
    public static void showLongToast(Context context,String msg){
        Toast toast = Toast.makeText(context,msg,Toast.LENGTH_LONG);
        toast.show();
    }
}
